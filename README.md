# README #

Baner editor
========================

Requirements:
---------------
* Will need [git](https://git-scm.com/download) to get project
* Will need [bower](https://bower.io/) to install dependencies

1.Download from git: 
```console
    git clone https://solodovnikov@bitbucket.org/solodovnikov/banner-editor.git
```
2.Install libs: 
```console
    bower install
```

What's inside?
--------------
1. [AngularJs](https://angularjs.org/) Framework
2. [RequireJs](http://requirejs.org/) - used for loading js files