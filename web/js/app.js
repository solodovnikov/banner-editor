'use strict';


require([
    'angular'
], function (angular) {
    require([
        'controllers/bannerCtrl',
        'directives/bnBoxCreator',
        'directives/bnDrag',
        'directives/bnResize'
    ], function (bannerCtrl, bnBoxCreatorDir, bnDragDir, bnResizeDir) {
        angular
            .module('app', [bnBoxCreatorDir, bnDragDir, bnResizeDir])
            .controller('bannerCtrl', bannerCtrl);
        angular.bootstrap(document, ['app']);
    });
});
