'use strict';

define([
        'angular'
    ], function (angular) {
        return ['$scope',
            function ($scope) {

                $scope.disabled = false;

                $scope.count = 0;

                $scope.selectedElementOptions = {
                    color: "#fcff1b"
                };

                var selectedElement;

                $scope.selected = false;

                $scope.removeBanner = function () {
                    selectedElement.remove();
                    if($scope.count == 10){
                        document.getElementById('addBanner').removeAttribute('disabled');
                    }
                    $scope.selected = false;
                    $scope.count--;

                };

                $scope.selectedChild = function ($event) {
                    selectedElement = $event.target.parentElement;
                    if (selectedElement.classList.contains('bannerEditorBox')) {
                        var container = selectedElement.getBoundingClientRect();
                        $scope.selectedElementOptions.width = container.width;
                        $scope.selectedElementOptions.height = container.height;
                        $scope.selectedElementOptions.newY = container.y;
                        $scope.selectedElementOptions.newX = container.x;
                        var color = window.getComputedStyle(selectedElement).backgroundColor;
                        var rgb = color.substring(4, color.length - 1).split(',');
                        $scope.selectedElementOptions.color = rgbToHex(parseInt(rgb[0]), parseInt(rgb[1]), parseInt(rgb[2]));
                    }
                };

                function componentToHex(c) {
                    var hex = c.toString(16);
                    return hex.length == 1 ? "0" + hex : hex;
                }

                function rgbToHex(r, g, b) {
                    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
                }

                $scope.$watch('selectedElementOptions', function (newVal) {
                    if (typeof selectedElement != 'undefined') {
                        var cssText = '';
                        if (selectedElement.classList.contains('selected')) {
                            cssText += 'width: ' + (newVal.width - 2) + 'px; ' + 'height: ' + (newVal.height - 2) + 'px; ';
                        } else {
                            cssText += 'width: ' + newVal.width + 'px; ' + 'height: ' + newVal.height + 'px; ';
                        }

                        cssText += 'background: ' + newVal.color + '; '
                                    + 'top: ' + newVal.newY + 'px; '
                                    + 'left: ' + newVal.newX + 'px; ';

                        selectedElement.style.cssText = cssText;
                    }
                }, true);

            }]
    }
)