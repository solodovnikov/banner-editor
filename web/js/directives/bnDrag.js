// To manage the drag
define([
    'angular'
], function (angular) {
    var moduleName = 'bnDragDirective';
    angular
        .module(moduleName, [])
        .directive("bnDrag", function ($document) {
            return function ($scope, $element, $attr) {
                var startX = 0, startY = 0, x, y;
                var newElement = angular.element('<div class="draggable"></div>');


                $element.append(newElement);
                newElement.on("mousedown", function ($event) {
                    event.preventDefault();

                    // To keep the last selected box in front
                    angular.element(document.querySelectorAll(".bannerEditorBox")).css("z-index", "0");
                    $element.css("z-index", "1");

                    startX = $event.pageX - $element[0].offsetLeft;
                    startY = $event.pageY - $element[0].offsetTop;
                    $document.on("mousemove", mousemove);
                    $document.on("mouseup", mouseup);
                });

                function mousemove($event) {
                    y = $event.pageY - startY;
                    x = $event.pageX - startX;
                    setPosition();
                }

                function mouseup() {
                    $document.off("mousemove", mousemove);
                    $document.off("mouseup", mouseup);
                }

                function setPosition() {
                    var container = document.getElementsByClassName('banner')[0].getBoundingClientRect();
                    var width = $element[0].offsetWidth,
                        height = $element[0].offsetHeight;

                    if (container) {
                        if (x < container.left) {
                            x = container.left;
                        } else if (x > container.right - width) {
                            x = container.right - width;
                        }
                        if (y < container.top) {
                            y = container.top;
                        } else if (y > container.bottom - height) {
                            y = container.bottom - height;
                        }
                    }

                    $element.css({
                        top: y + 'px',
                        left: x + 'px'
                    });
                }
            };
        })

    return moduleName;
});