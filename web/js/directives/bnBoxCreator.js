// To create a empty resizable and draggable box
define([
    'angular'
], function (angular) {
    var moduleName = 'bnBoxCreatorDirective';
    angular
        .module(moduleName, [])
        .directive("bnBoxCreator", function ($document, $compile) {
            return {
                restrict: 'A',
                $scope: {
                    count: '=',
                    selected: '='
                },
                link: function ($scope, $element, $attrs) {
                    $element.on("mousedown", function ($event) {
                        var newNode = $compile('<div class="bannerEditorBox" bn-click bn-drag bn-resize></div>')($scope);
                        var container = document.getElementsByClassName('banner')[0].getBoundingClientRect();
                        newNode.css({
                            top: (container.height - 75) / 2 + "px",
                            left: (container.width - 150) / 2 + "px",
                        });

                        $scope.count = parseInt($attrs.count) + 1;

                        if ($attrs.count == 9) {
                            $element.attr("disabled", "disabled");
                        }

                        newNode.on('click', function () {
                            var banners = document.getElementsByClassName('bannerEditorBox');
                            var size = banners.length;
                            for (var i = 0; i < size; i++) {
                                banners[i].classList.remove('selected');
                            }
                            $scope.selected = true;

                            if (this.classList.contains('selected') == false) {
                                this.classList.add('selected');
                            }

                            $scope.$apply();

                        });

                        $scope.$apply();


                        angular.element(document.getElementsByClassName('banner')).append(newNode);
                    });
                }
            }
        })
    return moduleName;
});